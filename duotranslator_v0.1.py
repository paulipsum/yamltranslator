from google.cloud import translate_v2
import os
import io
import yaml
import unidecode

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="duotranslation-b22a087265e1.json"

def translate_text(text,target='ro', source='en'):
    """
    Target must be an ISO 639-1 language code.
    https://cloud.google.com/translate/docs/languages
    """
    translate_client = translate_v2.Client()
    result = translate_client.translate(
        text,
        target_language=target, 
        source_language=source)
    return result['translatedText']

inputstream=open('inputyml2','r',encoding="utf8")
obyaml = yaml.safe_load(inputstream)


for cat in obyaml:
    for subcat in obyaml[cat]:
        obyaml[cat][subcat] = unidecode.unidecode(translate_text(str(obyaml[cat][subcat]),target='ro',source='en'))


outputstream=open('outyml2v2','w',encoding="utf8")
yaml.dump(obyaml, outputstream, encoding='utf-8')